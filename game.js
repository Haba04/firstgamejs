const canvas = document.getElementById('game')
const context = canvas.getContext('2d')

let flotEnemy = []
let timer = 0
let flot = {x: 300, y: 300}
let shot = []
let explosion = []
let countLife = 3
let gameOver

// Привязываем корабль к курсору мыши
canvas.addEventListener('mousemove', function (event) {
    flot.x = event.offsetX - 25
    flot.y = event.offsetY - 13
})

// const explosionAudio = new Audio()
// explosionAudio.src = './sounds/explosion.mp3'

const explosionImg = new Image()
explosionImg.src = './images/bah.png'

const shotImg = new Image()
shotImg.src = './images/shot.png'

const flotImg = new Image()
flotImg.src = './images/flot2.png'

const flotEnemyImg = new Image()
flotEnemyImg.src = './images/flotEnemy.png'

const fonImg = new Image()
fonImg.src = './images/fon.jpg'

fonImg.onload = () => {
    game()
}

function game() {

    render()
    gameOver = requestAnimFrame(game) // Браузер будет вызывать функцию game() 60 раз в секунду (60 Гц)
    update(gameOver)
}

function update(gameOver) {
    timer++

    // Создаем вражеские корабли
    if (timer % 10 == 0) {
        flotEnemy.push({
            x: Math.random() * 750,
            y: -50,
            dx: Math.random() * 2 - 1,
            dy: Math.random() * 2 + 2,
            delete: false
        })
    }

    // Создаем пули
    if (timer % 20 == 0) {
        shot.push({x: flot.x, y: flot.y, dx: 0, dy: -5})
        shot.push({x: flot.x, y: flot.y, dx: +0.3, dy: -5.3})
    }

    // Движение пуль
    for (let i in shot) {
        shot[i].x += shot[i].dx
        shot[i].y += shot[i].dy

        // Удаляем пули, вылетевшие за границу фона
        if (shot[i].y < -50) shot.splice(i, 1)
    }

    // Анимация взрывов
    for (let i in explosion) {
        explosion[i].animX += 0.6
        if (explosion[i].animX > 6) {
            explosion[i].animY++
            explosion[i].animX = 0
        }
        if (explosion[i].animY > 4)
            explosion.splice(i, 1)
    }

    // Физика
    for (let i in flotEnemy) {
        flotEnemy[i].x += flotEnemy[i].dx
        flotEnemy[i].y += flotEnemy[i].dy

        // Границы
        if (flotEnemy[i].x >= 750 || flotEnemy[i].x < 0) flotEnemy[i].dx = -flotEnemy[i].dx
        if (flotEnemy[i].y >= 800) flotEnemy.splice(i, 1)

        // Проверяем столкновение вражеских кораблей с ракетами
        for (let j in shot) {
            // Если произошло столкновение
            if (
                Math.abs(flotEnemy[i].x + 25 - shot[j].x - 25) < 50
                && Math.abs(flotEnemy[i].y - shot[j].y) < 25
            ) {
                // Добавляем в массив взрывы
                explosion.push({x: flotEnemy[i].x - 25, y: flotEnemy[i].y - 25, animX: 0, animY: 0})

                // Запускаем аудио взрыва
                new Audio('./sounds/explosion.mp3').play()

                // Отмечаем вражеский корабль, который надо удалить
                flotEnemy[i].delete = true
                shot.splice(j, 1)
                break
            }
        }

        // Проверяем столкновение нашего корабля с вражескими
        if (
            Math.abs(flotEnemy[i].x + 25 - flot.x - 30) < 55
            && Math.abs(flotEnemy[i].y + 25 - flot.y - 30) < 55
        ) {
            flotEnemy[i].delete = true
            countLife--
        }

        if (countLife === 0) {
            cancelAnimationFrame(gameOver)
        }

        if (flotEnemy[i].delete) flotEnemy.splice(i, 1)
    }

    context.font = '25px serif'
    context.fillStyle = 'white'
    context.fillText(`Колличество жизней: ${countLife}`, 10, 50)
}

// Отрисовка объектов
function render() {
    context.drawImage(fonImg, 0, 0, 800, 800)
    context.drawImage(flotImg, flot.x, flot.y, 60, 60)

    for (let j in shot)
        context.drawImage(shotImg, shot[j].x, shot[j].y, 50, 50)

    for (let i in flotEnemy)
        context.drawImage(flotEnemyImg, flotEnemy[i].x, flotEnemy[i].y, 50, 50)

    for (let k in explosion)
        context.drawImage(
            // explosionImg, 128 * Math.floor(explosion[k].animX), 128 * Math.floor(explosion[k].animY),
            // 128, 128, explosion[k].x, explosion[k].y, 80, 80

            explosionImg, 150 * Math.floor(explosion[k].animX), 150 * Math.floor(explosion[k].animY),
            150, 150, explosion[k].x, explosion[k].y, 80, 80
            )
}

// В зависимости от типа браузера
const requestAnimFrame = (function () {
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 20)
        }
})()